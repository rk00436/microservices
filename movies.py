from flask import Flask
from werkzeug.exceptions import NotFound
import json
import os

app = Flask(__name__)
with open("{}/database/movies.json".format(os.getcwd()), "r") as f:
    movies = json.load(f)


@app.route("/", methods=['GET'])
def hello():
    return json.dumps({
        "uri": "/",
        "subresource_uris": {
            "movies": "/movies",
            "movie": "/movies/<id>"
        }
    })

@app.route("/movies/<movieid>", methods=['GET'])
def movie_info(movieid):
    if movieid not in movies:
        raise NotFound

    result = movies[movieid]
    result["uri"] = "/movies/{}".format(movieid)

    return json.dumps(result)


@app.route("/movies", methods=['GET'])
def movie_record():
    return json.dumps(movies)


if __name__ == "__main__":
    app.run(port=5001, debug=True)

