#barebones makefile

clean: cleanmovieservice cleanstservice cleannetwork

all: movieservice stservice network

movieservice: buildmovieservice runmovieservice

buildmovieservice:
	docker build -t movies-img -f movieservice/Dockerfile .

runmovieservice:
	docker run --name movieservice -p 5001:5001 -itd movies-img

cleanmovieservice:
	-docker stop movieservice
	-docker rm movieservice
	-docker rmi movies-img


stservice: buildstservice runstservice

buildstservice:
	docker build -t stservice-img -f stservice/Dockerfile .

runstservice:
	docker run --name stservice -p 5002:5002 -itd stservice-img

cleanstservice: cleannetwork
	-docker stop stservice
	-docker rm stservice
	-docker rmi stservice-img

network:
	docker network create microservices-net
	docker network connect microservices-net stservice
	docker network connect microservices-net movieservice

cleannetwork:
	-docker network disconnect microservices-net stservice
	-docker network disconnect microservices-net movieservice
	-docker network rm microservices-net
