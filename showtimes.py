from flask import Flask
from werkzeug.exceptions import NotFound
import json
import requests
import os


app = Flask(__name__)
movies_service = "http://127.0.0.1:5001/movies/{}"
with open("{}/database/showtimes.json".format(os.getcwd()), "r") as f:
    showtimes = json.load(f)


@app.route("/", methods=['GET'])
def hello():
    return json.dumps({
        "uri": "/",
        "subresource_uris": {
            "showtimes": "/showtimes",
            "showtime": "/showtimes/<date>"
        }
    })


@app.route("/showtimes", methods=['GET'])
def showtimes_list():
    return json.dumps(showtimes)


@app.route("/showtimes/<date>", methods=['GET'])
def showtimes_record(date):
    if date not in showtimes:
        raise NotFound
    result = []
    for movie_id in showtimes[date]:
        resp = requests.get(movies_service.format(movie_id))
        result.append(resp.json()["title"])
    return json.dumps(result)

if __name__ == "__main__":
    app.run(port=5002, debug=True)
